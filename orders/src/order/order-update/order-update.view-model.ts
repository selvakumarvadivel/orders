import { makeObservable, observable } from 'mobx';
import { OrderUpdateViewModelInterface } from '../order.interface';
import OrderModel from '../order';

class OrderUpdateViewModel implements OrderUpdateViewModelInterface {
    order: OrderModel = new OrderModel();
    
    getOrder(orderID: string): OrderModel {
        throw new Error('Method not implemented.');
    }

    putOrder(orderID: string): void {
        throw new Error('Method not implemented.');
    }

    cancelOrder(orderID: string): void {
        throw new Error('Method not implemented.');
    }

}

export default OrderUpdateViewModel