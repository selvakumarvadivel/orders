import { makeObservable, observable } from 'mobx';
import { OrderUpdateViewControllerInterface } from '../order.interface';
import OrderModel from '../order';

class OrderUpdateViewController implements OrderUpdateViewControllerInterface {
    setPickUpLocation(location: string): void {
        throw new Error('Method not implemented.');
    }
    setDropOffLocation(location: string): void {
        throw new Error('Method not implemented.');
    }
    setPickUpDate(date: string): void {
        throw new Error('Method not implemented.');
    }
    setDropOffDate(date: string): void {
        throw new Error('Method not implemented.');
    }
}

export default OrderUpdateViewController