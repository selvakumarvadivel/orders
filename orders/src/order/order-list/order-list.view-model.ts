import { makeObservable, observable } from 'mobx';
import { OrderListViewModelInterface } from '../order.interface';
import OrderModel from '../order';

class OrderListViewModel implements OrderListViewModelInterface {
    orders: OrderModel[] = [];

    getOrders(): OrderModel[] {
        throw new Error('Method not implemented.');
    }
}

export default OrderListViewModel