import { makeObservable, observable } from 'mobx';
import { OrderViewViewModelInterface } from '../order.interface';
import OrderModel from '../order';

class OrderViewViewModel implements OrderViewViewModelInterface {
    order: OrderModel = new OrderModel();
    
    getOrder(orderID: string): OrderModel {
        throw new Error('Method not implemented.');
    }

    cancelOrder(orderID: string): void {
        throw new Error('Method not implemented.');
    }


}

export default OrderViewViewModel